******STACK DE OBSERVABILITY E MONITORAMENTO******

☸️configuração e instalação do kubernetes prometheus via K8s Skaffold
Configuração completa da pilha de monitoramento do Prometheus no Kubernetes.

Stack de Monitoramento/Observability

Instalar Appps na ordem via Skaffold

1 - Prometheus 

2 - Grafana

3 - Gerenciador de Alertas

4 - Node Exporter

5 - kube-state-metric (obs:caso necessitar de metricas do K8s Kubernetes)

Premissas

1- Instalar Binario Skaffold - https://skaffold.dev/docs/install/

2- Configurar o kube config para acesso ao k8s que será instalado a stack de Monitoramento

CLI Skaffold Commandos

A interface de linha de comando do Skaffold fornece os seguintes comandos:

skaffold run - para construir e implantar uma vez

skaffold dev - para acionar o fluxo de trabalho de compilação e implantação do loop de observação com limpeza na saída

skaffold debug - para executar um pipeline no modo de depuração

skaffold build - para apenas construir e marcar sua(s) imagem(ns)

skaffold deploy - para implantar a(s) imagem(ns) fornecida(s)

skaffold delete - para limpar os artefatos implantados

skaffold render - crie e marque imagens e gere manifestos do Kubernetes com modelos de saída

skaffold apply - para aplicar manifestos hidratados a um cluster

skaffold init - para inicializar a configuração do Skaffold

**Link de Referencia para Instalação: https://devopscube.com/setup-prometheus-monitoring-on-kubernetes/**
